import { createStackNavigator, createAppContainer } from 'react-navigation'
import ComponentPageScreen from '../Containers/ComponentPageScreen'
import PageScreen from '../Containers/PageScreen'
import LaunchScreen from '../Containers/LaunchScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  ComponentPageScreen: { screen: ComponentPageScreen },
  PageScreen: { screen: PageScreen },
  LaunchScreen: { screen: LaunchScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'PageScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)
